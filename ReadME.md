
## [Trello Clone](https://trello-dom-clone.netlify.app/)

This project is a simple Trello-like application built using HTML, Tailwind CSS, and JavaScript. It allows users to create boards and manage tasks in a visually appealing interface.


Screenshots:

![](./images/Screenshot-1.png)
![](./images/Screenshot-2.png)