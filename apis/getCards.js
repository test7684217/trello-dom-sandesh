import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function getCards(listId) {
  try {
    const response = await fetch(
      `https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );

    const cardList = await response.json();

    return cardList;



  } catch (error) {

    showToast(error.message,false);
  }
}
