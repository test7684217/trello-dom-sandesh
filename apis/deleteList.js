import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function archiveList(listId) {
  try {

    const response = await fetch(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${APIKey}&token=${token}`,
      {
        method: "PUT",
      }
    );

    const deleteList = await response.json();

    if(response.ok)
      showToast(`Selected list is deleted successfully`,true)
    
    return deleteList;

  } catch (error) {
    console.log(error);
    showToast(error.message,false);
  }
}
