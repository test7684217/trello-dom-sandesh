import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function getAllBoards() {
  try {
    const response = await fetch(
      `https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${token}`
    );
    
    const boards = await response.json();

    
    
    return boards;
  } catch (error) {
    showToast(error.message,false);
  }
}
