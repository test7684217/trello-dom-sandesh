import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function createCard(cardName,listId) {
  try {

    const response = await fetch(
      `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    );

    const newCard = await response.json();


    if(response.ok)
      showToast(`${cardName} card created successfully`,true)

    return newCard;
  } catch (error) {

    showToast(error.message,false)
  }
}
