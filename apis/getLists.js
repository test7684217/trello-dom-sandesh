import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function getLists(boardId) {
  try {
    const response = await fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${token}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );

    const list = await response.json();

    return list;
  } catch (error) {
    showToast(error.message,false);
  }
}
