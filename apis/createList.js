import { showToast } from "../utils.js";
import { token, APIKey } from "./constants.js";

export async function createList(listName, boardId) {
  try {
    const response = await fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
      }
    );

    const newList = await response.json();

    return newList;
  } catch (error) {
    showToast(error.message, false);
  }
}
