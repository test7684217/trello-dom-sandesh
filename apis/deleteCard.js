import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function archiveCard(id) {
  try {
    const response = await fetch(`https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${token}`, {
      method: "DELETE",
    });
    const data = await response.json();

    if(response.ok)
      showToast(`${boardName} board deleted successfully`,true)

    return data;
  } catch (error) {
    showToast(error.message,false)
  }
}
