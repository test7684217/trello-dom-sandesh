import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function createBoard(boardName) {
  try {
    const response = await fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${token}`,
      {
        method: "POST",
      }
    );
    const newBoard = await response.json();

    

    if(response.ok)
      showToast(`${boardName} board created successfully`,true)

    return newBoard;
  } catch (error) {
    showToast("error in creating board",false)
  }
}
