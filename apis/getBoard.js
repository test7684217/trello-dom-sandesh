import { showToast } from "../utils.js";
import { APIKey, token } from "./constants.js";

export async function getBoard(boardId) {
    try {
      const response = await fetch(
        `https://api.trello.com/1/boards/${boardId}?key=${APIKey}&token=${token}`,
        {
          method: "GET",
          headers: { Accept: "application/json" },
        }
      )
  
      const board = await response.json();

      return board;
  
    } catch (error) {
      showToast(error.message,false);
    }
  }
  