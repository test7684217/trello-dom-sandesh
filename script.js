import { createBoard } from "./apis/createBoard.js";
import { getAllBoards } from "./apis/getAllBoards.js";
import { showToast } from "./utils.js";

let boardsContainer = document.getElementById("boards-container");
let loader = document.getElementById("loader");

let currentTheme = localStorage.getItem("theme") || "light";
let themeButton = document.getElementById("theme");
let themeText = document.getElementById('theme-text');
let themeIcon = document.getElementById('icon')

let boardList = [];



function initializeTheme() {
  if (currentTheme === "dark") {
    document.documentElement.classList.add("dark");
    themeText.innerText = "Light Mode";
    themeIcon.classList.remove('fa-moon');
    themeIcon.classList.add('fa-circle');
  } else {
    document.documentElement.classList.remove("dark");
    themeText.innerText = "Dark Mode";
    themeIcon.classList.remove('fa-circle');
    themeIcon.classList.add('fa-moon');
  }
}

themeButton.addEventListener("click", () => {
  if (document.documentElement.classList.contains("dark")) {
    document.documentElement.classList.remove("dark");
    localStorage.setItem("theme", "light");
    themeText.innerText = "Dark Mode";

    themeIcon.classList.add('fa-moon');
    themeIcon.classList.remove('fa-circle');

  } else {
    document.documentElement.classList.add("dark");
    localStorage.setItem("theme", "dark");
    themeText.innerText = "Light Mode";

    themeIcon.classList.add('fa-circle');
    themeIcon.classList.remove('fa-moon');
  }
});

async function fetchBoards() {
  try {
    loader.classList.remove("hidden");
    boardList = await getAllBoards();
    loader.classList.add("hidden");
  } catch (error) {
    showToast("Failed to fetch boards");
  }
}

function renderBoards() {
  try {

    console.log(boardList)
    boardsContainer.innerHTML = "";

    const fragment = document.createDocumentFragment(); // reduces DOM updates => optimised

    boardList.forEach((board) => {
      let boardTitle = document.createElement("p");
      boardTitle.innerText = board.name;

      let boardNode = document.createElement("div");
      boardNode.append(boardTitle);
      boardNode.setAttribute(
        "class",
        "shadow-lg w-60 h-28 rounded-md flex items-center justify-center p-5 font-bold bg-white cursor-pointer text-blue-400 border-[1px] border-blue-400 hover:bg-blue-400 hover:text-white hover:scale-[1.05] hover:border-none dark:bg-blue-500 dark:text-white dark:border-blue-100 dark:shadow-darkShadow"
      );
      boardNode.addEventListener("click", () => {
        window.location.href = `board.html?id=${board.id}`;
      });

      fragment.append(boardNode);
    });

    boardsContainer.append(fragment);
  } catch (error) {

    console.log(error)
    showToast("Error in rendering boards",false);
  }
}

document.getElementById("openModalBtn").addEventListener("click", () => {
  document.getElementById("modal").classList.remove("hidden");
});

document.getElementById("closeModalBtn").addEventListener("click", () => {
  document.getElementById("modal").classList.add("hidden");
});

document.getElementById("closeModalBtn2").addEventListener("click", () => {
  document.getElementById("modal").classList.add("hidden");
});

window.addEventListener("click", (event) => {
  if (event.target === document.getElementById("modal")) {
    document.getElementById("modal").classList.add("hidden");
  }
});

document
  .getElementById("modalForm")
  .addEventListener("submit", async (event) => {
    event.preventDefault();
    document.getElementById("modal").classList.add("hidden");
    let boardName = document.getElementById("name").value;

    try {
      const newBoard = await createBoard(boardName);
      boardList.push(newBoard);
      renderBoards();
    } catch (error) {
      showToast("Failed to create board",false);
    }
  });

async function start() {
  initializeTheme();
  await fetchBoards();
  renderBoards();
}

start();


