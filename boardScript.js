import { createCard } from "./apis/createCard.js";
import { createList } from "./apis/createList.js";
import { archiveCard } from "./apis/deleteCard.js";
import { archiveList } from "./apis/deleteList.js";
import { getBoard } from "./apis/getBoard.js";
import { getCards } from "./apis/getCards.js";
import { getLists } from "./apis/getLists.js";
import { showToast } from "./utils.js";

const urlParams = new URLSearchParams(window.location.search);
const boardId = urlParams.get("id");

let listContainer = document.getElementById("lists-container");
let boardTitle = document.getElementById("board-title");
let addList = document.getElementById("add-list");
let loader = document.getElementById("loader");

let currentTheme = localStorage.getItem("theme") || "light";
let themeButton = document.getElementById("theme");


let themeText = document.getElementById('theme-text');
let themeIcon = document.getElementById('icon')


function initializeTheme() {
  if (currentTheme === "dark") {
    document.documentElement.classList.add("dark");
    themeText.innerText = "Light Mode";
    themeIcon.classList.remove('fa-moon');
    themeIcon.classList.add('fa-circle');
  } else {
    document.documentElement.classList.remove("dark");
    themeText.innerText = "Dark Mode";
    themeIcon.classList.remove('fa-circle');
    themeIcon.classList.add('fa-moon');
  }
}

themeButton.addEventListener("click", () => {
  if (document.documentElement.classList.contains("dark")) {
    document.documentElement.classList.remove("dark");
    localStorage.setItem("theme", "light");
    themeText.innerText = "Dark Mode";

    themeIcon.classList.add('fa-moon');
    themeIcon.classList.remove('fa-circle');

  } else {
    document.documentElement.classList.add("dark");
    localStorage.setItem("theme", "dark");
    themeText.innerText = "Light Mode";

    themeIcon.classList.add('fa-circle');
    themeIcon.classList.remove('fa-moon');
  }
});

// Show the modal when clicking "Add List"
addList.addEventListener("click", () => {
  document.getElementById("addListModal").classList.remove("hidden");
});

// Hide the modal when clicking the close button
document.querySelector(".close").addEventListener("click", () => {
  document.getElementById("addListModal").classList.add("hidden");
});

// Hide the modal when clicking outside of it
window.addEventListener("click", (event) => {
  const modal = document.getElementById("addListModal");
  if (event.target == modal) {
    modal.classList.add("hidden");
  }
});

// Handle adding a list when the modal form is submitted
document.getElementById("addListBtn").addEventListener("click", async () => {
  const listName = document.getElementById("listNameInput").value;
  await handleAddList(listName);
  document.getElementById("addListModal").classList.add("hidden");
});
document.getElementById("back").addEventListener("click", () => {
  window.history.back();
});

let lists = [];

async function fetchBoardAndLists() {
  try {
    loader.style.display = "block";
    const boardData = await getBoard(boardId);
    boardTitle.setAttribute(
      "class",
      "border-blue-400 dark: border-blue-100  border-[2px] text-blue-400 dark:text-white bg-white dark:bg-blue-500 text-[18px] font-bold rounded-tr-[50px] rounded-br-[50px] p-5"
    );
    boardTitle.innerText = boardData.name;

    lists = await getLists(boardId);
    renderLists();
  } catch (error) {
    console.error("Error fetching board or lists:", error);
    showToast("Failed to load board or lists");
  } finally {
    loader.style.display = "none";
  }
}

async function renderLists() {
  try {
    loader.style.display = "block";
    listContainer.innerHTML = "";

    let listPromises = lists.map(async (list) => {
      let listHeader = document.createElement("div");
      listHeader.setAttribute("class", "w-full flex justify-between p-2");

      let listTitle = document.createElement("p");
      listTitle.setAttribute("class", "text-white hover:text-white");
      listTitle.innerText = list.name;

      let deleteListButton = document.createElement("i");
      deleteListButton.setAttribute(
        "class",
        "fa-solid fa-trash cursor-pointer hover:text-red-500"
      );

      deleteListButton.addEventListener("click", async () => {
        await deleteList(list.id);
      });

      let listNode = document.createElement("div");
      listNode.setAttribute(
        "class",
        "bg-blue-500 dark:bg-black min-w-96 h-fit rounded-md flex flex-col items-center p-5 text-white gap-2 shadow-lg hover:opacity-[0.9] pt-2 border-[1px] dark:border-blue-500"
      );

      let cardsContainer = document.createElement("div");
      cardsContainer.setAttribute(
        "class",
        "flex flex-col w-full overflow-y gap-2"
      );

      listHeader.append(listTitle);
      listHeader.append(deleteListButton);

      listNode.append(listHeader);

      createAddCardButton(listNode, list.id);

      try {
        const cardList = await getCards(list.id);

        cardList.forEach((card) => {
          let cardNode = createCardNode(card.name, card.id);
          cardsContainer.append(cardNode);
        });
      } catch (error) {
        showToast(error);
        showToast("Error in getting cards");
      }
      listNode.append(cardsContainer);

      return listNode;
    });

    let lists1 = await Promise.all(listPromises);

    lists1.forEach((list) => {
      listContainer.append(list);
    });

    loader.style.display = "none";
  } catch (error) {
    showToast(error.message, false);
  }
}

function createCardNode(title, cardId) {
  let cardNode = document.createElement("div");
  cardNode.setAttribute(
    "class",
    "flex p-4 shadow-lg border-blue-500 border-[1px] bg-white dark:bg-blue-500 text-red-500  w-full rounded-[20px] p-2 flex justify-between items-center cursor-pointer"
  );
  let cardTitle = document.createElement("p");
  cardTitle.setAttribute("class", "text-black dark:text-white");
  cardTitle.innerText = title;

  let deleteCardButton = document.createElement("i");
  deleteCardButton.setAttribute("class", "fa-solid fa-trash cursor-pointer");

  deleteCardButton.addEventListener("click", async () => {
    await deleteCard(cardId);
  });

  cardNode.append(cardTitle);
  cardNode.append(deleteCardButton);
  return cardNode;
}

function createAddCardButton(listNode, listId) {
  let addCard = document.createElement("input");
  addCard.placeholder = "Add a card";
  addCard.setAttribute(
    "class",
    "w-full p-2 text-black dark:text-white dark:bg-black rounded-md dark:border-blue-500 border-[1px]"
  );

  addCard.addEventListener("keydown", async (event) => {
    if (event.key == "Enter") {
      await handleAddCard(addCard.value, listId);
    }
  });

  listNode.append(addCard);
}


async function handleAddCard(cardName, listId) {
  try {
    await createCard(cardName, listId);
    await fetchBoardAndLists();
  } catch (error) {
    console.error("Error creating card:", error);
    showToast("Failed to create card");
  }
}

async function handleAddList(listName) {
  try {
    const newList = await createList(listName, boardId);
    lists.push(newList);
    renderLists();
  } catch (error) {
    console.error("Error creating list:", error);
    showToast("Failed to create list");
  }
}

async function deleteList(listId) {
  try {
    let deletedList = await archiveList(listId);
    lists = lists.filter((list) => list.id != listId);
    renderLists();
  } catch (error) {
    showToast("Delete list error");
  }
}

async function deleteCard(cardId) {
  try {
    await archiveCard(cardId);
    renderLists();
  } catch (error) {
    showToast("Delete card error");
  }
}

// Initialize the board and lists on page load
fetchBoardAndLists();
initializeTheme();
