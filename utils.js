export function showToast(message,status) {
    const toastContainer = document.getElementById("toastContainer");
  
    const toast = document.createElement("div");
    
    if(status)
        toast.classList.add("toast-success");
    else
        toast.classList.add("toast-fail")

    toast.textContent = message;
  
    toastContainer.appendChild(toast);
  
    setTimeout(() => {
      toast.remove();
    }, 3000);
  }
  



  