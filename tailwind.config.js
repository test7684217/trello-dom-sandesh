tailwind.config = {
  darkMode: 'selector',
  theme: {
    extend: {
      colors: {
        backgroundDark: "black" ,
      },
      boxShadow:{
        darkShadow: 'rgba(38, 157, 255, 1) 0px 0px 15px 0px'
      }
    }
  }
}